require 'httparty'
require 'json'

class Weathertrend
  include HTTParty
  format :json
  default_timeout 30
  
  class MissingAPIKey < RuntimeError; end

  attr_accessor :api_key

  def initialize(api_key = nil, extra_params = {})
    @api_key = api_key || ENV['WEATHERTRENDS_APIKEY'] || self.class.api_key
  end
  
  def base_api_url
    "http://weathertrends.herokuapp.com/api/v1/"
  end
  def trend(date,location)
    call base_api_url + "trends/#{date}/q/#{location.gsub(" ","_")}"
  end
  
  def location(lat,lon)
    call base_api_url + "locations/#{lat},#{lon}"
  end
  
  def snapshot(date,location)
    call base_api_url + "snapshots/#{date}/q/#{location.gsub(" ","_")}"
  end
  
  protected
  
    def call(url)
      raise MissingAPIKey if @api_key.nil?
      response = self.class.get(url << "?apikey=#{@api_key}")
      begin
        response = JSON.parse(response.body)
      rescue
        response = response.body
      end
      response
    end

end

