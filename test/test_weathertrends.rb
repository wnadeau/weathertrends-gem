require 'helper'
require 'cgi'
require 'addressable/uri'

class TestWeathertrends < Test::Unit::TestCase

  def apikey
    "123"
  end
  def apikey_param
    "?apikey=#{apikey}"
  end

  context "api url" do
    setup do
      @weathertrends = Weathertrend.new(apikey)
      @url = "http://weathertrends.herokuapp.com/api/v1/"
    end
    
    should "raise exception at empty api key" do
      @weathertrends.api_key=nil
      assert_raise Weathertrend::MissingAPIKey do
        @weathertrends.trend(Time.now.to_date,"CA/San Fransisco")
      end
      @weathertrends.api_key = apikey
    end

    should "contain api key" do
      expect_get(@url+"trends/#{Time.now.to_date}/q/ME/Portland")
      @weathertrends.trend(Time.now.to_date,"ME/Portland")
    end
    
    context 'locations method url' do
       should 'wraps a lat/lon coords' do
         expect_get(@url+"locations/34.0,-84.0")
         @weathertrends.location(34.0,-84.0)
       end
     end
     context 'snapshots method url' do
       should 'wraps a date and a location query' do
         expect_get(@url+"snapshots/#{Time.now.to_date}/q/ME/Portland")
         @weathertrends.snapshot(Time.now.to_date,"ME/Portland")
       end
     end

  end
  
 
  private

    def expect_get(expected_url,expected_options={})
      Weathertrend.expects(:get).with{|url|
        url == expected_url + apikey_param
      }.returns(Struct.new(:body).new("") )
    end
end
